package com.example.hp.student;

import java.util.Comparator;

/**
 * Created by HP on 9/11/2015.
 */
public class Byrollno implements Comparator<Studentdata> {
    @Override
    public int compare(Studentdata lhs, Studentdata rhs) {
        long firstroll = lhs.getRollno();
        long secondroll = rhs.getRollno();

        if (firstroll > secondroll)
        {
            return 1;
        }
        else
        {
            return -1;
        }


    }
}
