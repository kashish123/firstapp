package com.example.hp.student;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.hp.student.Student1;
public class Student1 extends Activity implements View.OnClickListener {
    EditText name = null;
    EditText rollno = null;
    EditText phone = null;
    EditText email = null;

    Studentdata stud;
    boolean isEditable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student1);


        name = (EditText) findViewById(R.id.name);
        rollno = (EditText) findViewById(R.id.rollno);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);


        final Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        final Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);

        if (getIntent().hasExtra("vi")) {
            stud = (Studentdata) getIntent().getSerializableExtra("vi");
            isEditable = getIntent().getBooleanExtra("edit", true);
        }
        if (stud != null) {
            name.setText(stud.getName());
            rollno.setText(String.valueOf(stud.getRollno()));
            phone.setText(String.valueOf(stud.getPhone()));
            email.setText(stud.getEmail());


            name.setEnabled(isEditable);
            rollno.setEnabled(isEditable);
            phone.setEnabled(isEditable);
            email.setEnabled(isEditable);


            if (!isEditable) {
                save.setVisibility(View.INVISIBLE);
            } else {
                save.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_student1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        EditText name = (EditText) findViewById(R.id.name);
        String nameis = name.getText().toString();

        EditText rollno = (EditText) findViewById(R.id.rollno);
        String roll1 = rollno.getText().toString();

        EditText phone = (EditText) findViewById(R.id.phone);
        String phoneno1 = phone.getText().toString();

        EditText email = (EditText) findViewById(R.id.email);
        String enteremail1 = email.getText().toString();


        switch (v.getId()) {

            case R.id.save:
                if ((!nameis.isEmpty()) && (!roll1.isEmpty()) && (!phoneno1.isEmpty() && (!enteremail1.isEmpty())))

                {
                    Intent in = new Intent();

                    if (isEditable) {
                        stud.setName(nameis);
                        stud.setRollno(Long.parseLong(roll1));
                        stud.setPhone(Long.parseLong(phoneno1));
                        stud.setEmail(enteremail1);
                        in.putExtra("vi", stud);
                    }


                    in.putExtra("name",nameis);
                    in.putExtra("roll",roll1);
                    in.putExtra("phoneno",phoneno1);
                    in.putExtra("email",enteremail1);

                    setResult(RESULT_OK, in);
                    finish();
                }
                break;


            case R.id.cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;


        }

    }


}
