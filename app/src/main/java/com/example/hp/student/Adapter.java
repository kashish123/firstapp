package com.example.hp.student;
import java.util.List;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by HP on 9/9/2015.
 */
public class Adapter extends BaseAdapter
{

  List<Studentdata> data;
  Context context;
int rowlayout;


    public Adapter(List<Studentdata> data,Context context,int rowlayout )
    {
        this.data=data;
        this.context=context;
        this.rowlayout=rowlayout;
    }




    @Override
    public int getCount() {
        return data == null?0:data.size();

    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {return position;}



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

convertView= LayoutInflater.from(context).inflate(rowlayout, null);
        TextView textName=(TextView) convertView.findViewById(R.id.t1);
        textName.setText(data.get(position).getName());
        TextView textRoll=(TextView) convertView.findViewById(R.id.t2);
        textRoll.setText(String.valueOf(data.get(position).getRollno()));
        TextView textPhone=(TextView) convertView.findViewById(R.id.t3);
        textPhone.setText(String.valueOf(data.get(position).getPhone()));
        TextView textEmail=(TextView) convertView.findViewById(R.id.t4);
        textEmail.setText(data.get(position).getEmail());

        return convertView;
    }
}
