package com.example.hp.student;




import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;
//import android.view.OnLi

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;


public class MainActivity extends Activity implements DialogListener {


    Adapter adapter;
    Intent intentobj;
    LinkedList<Studentdata> ldata;
    public int positionedclicked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button adddetail = (Button) findViewById(R.id.detail);
        //adddetail.setOnClickListener(this);

        final ListView lv = (ListView) findViewById(R.id.listView);

        final GridView gv = (GridView) findViewById(R.id.gridView);
        ldata = new LinkedList<>();

        adapter = new Adapter(ldata, this, R.layout.activity_studentdisplay);
        lv.setAdapter(adapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                positionedclicked = position;
                DialogBox dialog = new DialogBox();
                dialog.setDialogListener(MainActivity.this);
                dialog.show(getFragmentManager(), "Dialog");

            }
        });


        adddetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterName();

            }
        });

        Button listv = (Button) findViewById(R.id.list);
        listv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lv.setVisibility(View.VISIBLE);
                gv.setVisibility(View.INVISIBLE);
                lv.setAdapter(adapter);


                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        positionedclicked = position;
                        DialogBox dialog = new DialogBox();
                        dialog.setDialogListener(MainActivity.this);
                        dialog.show(getFragmentManager(), "Dialog");

                    }
                });


            }
        });


        final Button gridv = (Button) findViewById(R.id.grid);
        gridv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lv.setVisibility(View.INVISIBLE);
                gv.setVisibility(View.VISIBLE);
                gv.setAdapter(adapter);


                gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        positionedclicked = position;
                        DialogBox dialog = new DialogBox();
                        dialog.setDialogListener(MainActivity.this);
                        dialog.show(getFragmentManager(), "Dialog");

                    }
                });

            }
        });

       final Spinner spin=(Spinner)findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(new OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String value = spin.getSelectedItem().toString();
                if (value.equals("Name"))
                {
                    Collections.sort(ldata, new Byname());
                    adapter.notifyDataSetChanged();
                }
                else if (value.equals("Rollno"))

                {
                    Collections.sort(ldata, new Byrollno());
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void enterName() {
        intentobj = new Intent(this, Student1.class);
        startActivityForResult(intentobj, 1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Studentdata obj = new Studentdata();
        if ((resultCode == RESULT_OK)) {
            switch (requestCode) {
                case 1:
                    obj.name = data.getStringExtra("name");
                    obj.rollno = Long.parseLong(data.getStringExtra("roll"));
                    obj.phone = Long.parseLong(data.getStringExtra("phoneno"));
                    obj.email = data.getStringExtra("email");
                    ldata.add(obj);
                    adapter.notifyDataSetChanged();
                    break;


                case 10:
                    ldata.set(positionedclicked, (Studentdata) data.getSerializableExtra("vi"));
                    break;

                case 20:
                    ldata.set(positionedclicked, (Studentdata) data.getSerializableExtra("vi"));
                    adapter.notifyDataSetChanged();
                    break;

                case 30:
                    ldata.set(positionedclicked, (Studentdata) data.getSerializableExtra("vi"));
                    break;

            }
        }

    }


    public void OnClick(int position) {
        switch (position) {
            case 1:

                Intent in = new Intent(this, Student1.class);
                in.putExtra("vi", ldata.get(positionedclicked));
                in.putExtra("edit", false);
                startActivityForResult(in, 10);
                break;


            case 2:

                Intent in1 = new Intent(this, Student1.class);
                in1.putExtra("vi", (Serializable) ldata.get(positionedclicked));
                in1.putExtra("edit", true);
                startActivityForResult(in1, 20);
                break;

            case 3:

                ldata.remove(positionedclicked);
                adapter.notifyDataSetChanged();
                break;


        }

    }
}