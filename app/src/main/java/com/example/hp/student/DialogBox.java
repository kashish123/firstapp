package com.example.hp.student;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
//import android.content.DialogListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import com.example.hp.student.DialogBox;

/**
 * Created by HP on 9/10/2015.
 */
public class DialogBox extends DialogFragment implements View.OnClickListener {
    LayoutInflater inflater;
    View v;
    DialogListener dialogL;

    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        inflater=getActivity().getLayoutInflater();
        v=inflater.inflate(R.layout.dialog, null);
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setView(v);

        final Button vi=(Button) v.findViewById(R.id.vi);
        vi.setOnClickListener(this);
        Button edit=(Button) v.findViewById(R.id.edit);
        edit.setOnClickListener(this);
        Button delete=(Button) v.findViewById(R.id.delete);
        delete.setOnClickListener(this);
        return builder.create();


    }




    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.vi:
                dialogL.OnClick(1);
                dismiss();
                break;


            case R.id.edit:
                dialogL.OnClick(2);
                dismiss();
                break;


            case R.id.delete:
                dialogL.OnClick(3);
                dismiss();
                break;
        }
    }

    public void setDialogListener(DialogListener dialogL)
    {
        this.dialogL=dialogL;
    }

}
