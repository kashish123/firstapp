package com.example.hp.student;

import java.util.Comparator;

/**
 * Created by HP on 9/11/2015.
 */
public class Byname implements Comparator<Studentdata>
{


    @Override
    public int compare(Studentdata lhs, Studentdata rhs) {

        String firstname=lhs.getName();
        String secondname=rhs.getName();


        if(firstname.compareToIgnoreCase(secondname)>0)
        {
            return  1;
        }
else
        {
            return -1;
        }


    }
}
